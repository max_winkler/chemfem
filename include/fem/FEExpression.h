#ifndef _FE_EXPRESSION_H_
#define _FE_EXPRESSION_H_

#include <vector>

#include "linalg/Vector.h"

namespace chemfem{
  namespace fem{

    typedef double (*ScalarFunction)(double, double);
    typedef chemfem::linalg::Vector (*VectorFunction)(double, double);
    
    enum ExpressionType {SECOND_ORDER, FIRST_ORDER, ZERO_ORDER, VOLUME_FORCE, NEUMANN_BC,
      VOLUME_RESIDUAL, EDGE_JUMP};

    /**
     * This class is used to store a single term in a partial differential equation.
     * Here, we distinguish among 2nd, 1st and zero-order terms. 
     * Coefficients belonging to the terms are stored in the class as well.
     */
    class FEExpression
    {
      friend class BilinearForm;
      friend class LinearForm;
      
    public:
      FEExpression(ExpressionType); 
      FEExpression(ExpressionType, ScalarFunction);

      // \todo These functions are added to avoid these friend declarations. Use these functions in BilinearForm and LinearForm too.
      ExpressionType GetType() const;
      double EvalCoeff(double x, double y) const;
        
    private:
      ExpressionType Type;
      ScalarFunction Coeff;
    };
    
  };
};

#endif
